 function [anchor_angles,sag,xr,yr,F_tension] = wire_sim(endforce,ver_dist,hor_dist,D_wr,wpm_wr,ch_weight, ch2ch_dist ,n_nodes)
    clear xr yr results 
     % not a great idea to do inside a function, but we are going to be calling it through an API for other purposes anyway
    % xr and yr are the resulting x and y coordinates of the rope points 
    % endforce is the force on the right end of the wire rope [N]
    % the diameter of the wire rope, D_wr, is expected to be given in [mm]

    %% value inputs
    n_sections = n_nodes -1 ; %Number of sections calculated
    E = 200e9; % Wire elasticity modulus
    fill_factor = 0.46 ; % afactor to represent the fact that the wire rope cross-section is not all metal
    A = fill_factor * pi * (D_wr/1000/2)^2 ; % wire rope cross section [m^2]
    L = sqrt(hor_dist^2 + ver_dist^2); % Distance between the last sheave of the left pillar and the first of the right [m]
    N0 = wpm_wr * L; % Cable weight for the spanned length


%     endforce = 100e3 ; 


    %% preparations 
    % Distributed load due to wire rope weight 
    x0 = L/n_sections;
    N = zeros (1, n_sections-1);
    for i = 1:n_sections-1
        N(1, i) = N0/(n_sections-1);
    end

    % point loads due to chair weight 
    R = ch2ch_dist/x0 ; % Distance between carriages (measured in nodes)
    R = floor(R); 
    for i = 1:n_sections-1
        if (mod(i, R) == 0)
            N(1, i) = N(1, i) + ch_weight;
        end
    end

    %% value prediction
    pred = zeros(1, 2*n_sections+1);
    for i = 1:2*n_sections
        if(i<n_sections+1)
        pred(1, i) = -1 + 2*(i-1)/(n_sections-1);
        else
        pred(1, i) = x0;
        end
    end
    pred(1, 2*n_sections+1) = 1;

    %% equations
    syms phi x [1, n_sections]
    syms L0
    for i = 1:n_sections-1
        eqn(1, i) = -E*A*sin(phi(1, i))*(x(1, i)*sqrt(1+tan(phi(1, i))^2) - L0) + E*A*sin(phi(1, i+1))*(x(1, i+1)*sqrt(1+tan(phi(1, i+1))^2) - L0) - N(1, i)*L0;
        eqn(1, n_sections-1+i) = cos(phi(1, i))*(x(1, i)*sqrt(1+tan(phi(1, i))^2) - L0) - cos(phi(1, i+1))*(x(1, i+1)*sqrt(1+tan(phi(1, i+1))^2) - L0);
    end
    eqn(2*n_sections-1) = x(1, 1) - sqrt(L^2 - ver_dist^2);
    for i = 2:n_sections
    eqn(2*n_sections-1) = eqn(2*n_sections-1) + x(1, i);
    end
    eqn(2*n_sections) = x(1, 1)*tan(phi(1, 1)) - ver_dist;
    for i = 2:n_sections
    eqn(2*n_sections) = eqn(2*n_sections) + x(1, i)*tan(phi(1, i));
    end
    eqn (2*n_sections+1) = E*A*(x(1, n_sections)*sqrt(1+tan(phi(1, n_sections))^2) - L0)/L0 - endforce;

    %% solver
    sol = vpasolve(eqn, [phi, x, L0], pred);
%     disp(sol)
    first_angle = sol.(string(phi(1))); 
    last_angle = sol.(string(phi(n_sections)));
    anchor_angles = [double(first_angle),double(last_angle)] ; % wire rope angle at the anchor points [lower point,higher point]

    %% post - processing
    results = double(table2array(struct2table(sol)));
    xr = zeros(1, n_sections+1);
    yr = zeros(1, n_sections+1);
    k = zeros(1, n_sections+1);
    for i = 1:n_sections
        disp(i)
        xr(1, i+1) = results(1, n_sections+i) + xr(1 , i); 
        yr(1, i+1) = results(1, n_sections+i)*tan(results(1, i)) + yr(1 , i);
        k(1, i+1) = results(1, n_sections+i)*tan(results(1, i)) + k(1 , i);
    end

    % evaluate sag 
    sag = abs(min(k)) ; % the first anchor point is at height 0. This metric shows the "unecessary" vertical travel


    % plot the resulting wire rope shape
    % plot (xr,yr)
    % ylim([-L/2, L/2])

    %% result checker - displacement calculation
    rphi = zeros(1,n_sections);
    rx = zeros(1,n_sections);
    for i = 1:n_sections
        rphi(i) = results(1, i);
        rx (i) = results(1, n_sections+i);
    end

    rL0 = results(1, 2*n_sections+1);
    for i = 1:n_sections
        % disp(sqrt(rx(i)^2+(rx(i)*tan(rphi(i)))^2) - rL0)
        if ((sqrt(rx(i)^2+(rx(i)*tan(rphi(i)))^2) - rL0)<0)
            disp('FAIL')
        end
    end

    %% Forces
    F_tension = zeros(1,n_sections); 
    for i = 1:n_sections
        F_tension(i) = E*A*(sqrt(rx(i)^2+(rx(i)*tan(rphi(i)))^2) - rL0)/rL0 ; 
        % disp(i)
        % disp(E*A*(sqrt(rx(i)^2+(rx(i)*tan(rphi(i)))^2) - rL0)/rL0)
    end

    
    % disp("L difference")
    % disp(rL0*n_sections-L)

    % disp(max(F_tension))

end 