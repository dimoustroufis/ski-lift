## Code files 
The files `liftline.py` and `liftline.ipynb` actually contain the same calculations, but the former is in  a continuous 
code format, allowing for loops over the entire solution and for iterrativce processes.

## Variable naming conventions for the python codes
* The letter "p" between 2 other letters indicates the word "per". For example "wpm" is "weight per meter"
* Number of something is preposed with `n_`
* Efficiencies are preposed with `eff_`
* Safety factors are preposed with `sf_`. For example `sf_n_carts`
* Loads are preposed with `ld_`. Loads are basically forces, but refer to a more system-wide approach. For example the load from all the lift chairs.
* Diameters are preposed with `D_`
* Torques are preposed with `T_`
* Froces are preposed with `F_`
* Young's modulus is denoted with `E_`
* Length is preposed with `L_`
* Width is preposed with `W_`
* Weight is preposed with `w_`