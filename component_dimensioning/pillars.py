import numpy as np 
import matplotlib.pyplot as plt 

class pillar_section(): 
    # to compose the pillar in code and calculate quantities needed by the TIA-222-G standard
    def __init__(self,A_proj, D ,ispole = False , isround = False, shape = 'square'): 
        self.A_proj = A_proj
        self.D = D  
        self.shape = shape # can be either 'square' or 'triangular'
        self.ispole = ispole # to determine if the member is a pole (the main body of the pillar) or not 
        self.isround = isround # flag to determine if we consider it as a round component or not 

    def calc_Rr(self,epsilon,C): 
        if (C<4.4): 
            R_r = min([0.57 - 0.14*epsilon + 0.86*epsilon**2 - 0.24*epsilon**3, 1])
        elif (C > 8.7): 
            R_r = 0.36 + 0.26*epsilon + 0.97*epsilon**2 - 0.63*epsilon**3 
        
        # linear interpolation for the transitional flow regime
        else : 
            R_r_subcritical = min([0.57 - 0.14*epsilon + 0.86*epsilon**2 - 0.24*epsilon**3, 1])
            R_r_supercritical = 0.36 + 0.26*epsilon + 0.97*epsilon**2 - 0.63*epsilon**3 
            R_r = np.interp(C,[4.4,8.7],[min(R_r_subcritical,R_r_supercritical), max(R_r_subcritical,R_r_supercritical)])
        return R_r 

    def calc_Cf(self,u_wind,factors,epsilon=0): 
        # factors = [importance factor (a float not a list), K_zt, K_z]
        # epsilon is the solidity ratio and is the fraction of the projected area to the area of the face as if it were solid
        C = (factors[0]*factors[1]*factors[2])**0.5 * u_wind * self.D
        C_f = 0 

        # hard coded C matrices from the TIA-222
        if (self.ispole == True) : 
                # for round parts
            if (C<4.4): 
                C_f = 1.2 
            if (C >= 4.4 and C <= 8.7): 
                C_f = 5.23/C
            if(C > 8.7): 
                C_f = 0.6 
        else : 
            if (self.shape == 'square'): 
                C_f = 4*epsilon**2 - 5.9*epsilon + 4 
            elif (self.shape == 'triangular'):
                C_f = 3.4*epsilon**2 - 4.7*epsilon + 3.4
        return C_f,C 
    
    def calc_Df(self,epsilon=0,wind_dir = 'norm'):
        # wind dir can be 'norm' and '45' for square members and 'norm','60','90' for triangular members
        # this function does not take round members (as defined by the standard into account)
        if(self.shape == 'square'): 
            if (wind_dir == 'norm'): 
                D_f = 1 
            elif (wind_dir == '45'):
                D_f = min([1.2, 1 + 0.75*epsilon])
        
        elif(self.shape == 'triangular'): 
            if (wind_dir == 'norm'): 
                D_f = 1 
            elif (wind_dir == '60'): 
                D_f = 0.8 
            elif(wind_dir == '90'): 
                D_f = 0.85 
        
        return D_f 
    
    def calc_EPA(self,factors,u_wind,wind_dir = 'norm',epsilon = 0): 
        # calculates the Effective Projected Area 
        C_f,C = self.calc_Cf(u_wind,factors,epsilon)
        D_f = self.calc_Df(epsilon,wind_dir)  # for square sections, Df is the same for flat and round
        if (self.isround == False):
            R_r = 1 
        else : 
            R_r = self.calc_Rr(epsilon,C)
        
        print(R_r)
        EPA = C_f*D_f*R_r*self.A_proj
        return EPA



                


class pillar_group(): 

    def __init__(self,problem_data, user_data): 
        # problem data contains all problem-specific data, stemming from the project desctiption
        u_cart = problem_data[0]
        pph = problem_data[1]
        ppc = problem_data[2]
        self.tot_ver_diff = problem_data[3]
        self.tot_hor_diff = problem_data[4]
        
        self.wpm_wr = user_data[0]
        wpp = user_data[1]
        wpc_dry = user_data[2]
        self.wpc_wet = user_data[3]
        self.pillar_height = user_data[4]
        self.height_margin = user_data[5]
        self.ch2ch_dist = user_data[6] 
        self.slope = np.arctan(self.tot_ver_diff/self.tot_hor_diff)

    def number(self,F_tension_uppermost,D_wr,sag_percent,matlab_engine,n_init = 2, n_nodes = 40) : 
        # Besides the number of pillars needed, this function also returns the resulting length of the wire rope
        # sag_percent to be given as decimal (0.05 for 5%)
        # the mimimum number of pillars is 2, which represents the top and bottom stations 
        # F_tension_uppermost is the tension on the uppermost sheave. The MATLAB code needs this as a boundary condition

        # eng = matlab.engine.start_matlab()
        n_pillars = n_init 
        hor_dist = self.tot_hor_diff / (n_pillars-1)
        ver_dist = self.tot_ver_diff / (n_pillars-1)
        anchor_angles,sag,x_points_wr,y_points_wr,F_tension = matlab_engine.wire_sim(F_tension_uppermost,ver_dist,hor_dist,D_wr,self.wpm_wr,self.wpc_wet,self.ch2ch_dist,n_nodes,nargout= 5) # select the elements with anchor_angles[0][n]
        

        while(sag > hor_dist*sag_percent or sag > (self.pillar_height - self.height_margin)) : 
            # print("Sag too big, pillar number increased ...")
            n_pillars += 1 
            hor_dist = self.tot_hor_diff / (n_pillars-1)
            ver_dist = self.tot_ver_diff / (n_pillars-1)

            # eng = matlab.engine.start_matlab()
            anchor_angles,sag,x_points_wr,y_points_wr,F_tension = matlab_engine.wire_sim(F_tension_uppermost,ver_dist,hor_dist,D_wr,self.wpm_wr,self.wpc_wet,self.ch2ch_dist,n_nodes,nargout= 5)
            # print("sag = %lf"%sag)   
            print("Number of pillars : %lf"%n_pillars)
        
        # Convert MATLAB alien variables to human 
        F_tension = [float(num) for num in F_tension[0]]
        anchor_angles = [float(num) for num in anchor_angles[0]]
        x_points_wr = [float(num) for num in x_points_wr[0]]
        y_points_wr = [float(num) for num in y_points_wr[0]]

        length = 0.0
        for i in range(1, len(x_points_wr)):
            dx = x_points_wr[i] - x_points_wr[i-1]
            dy = y_points_wr[i] - y_points_wr[i-1]
            length = length + np.sqrt(dx*dx + dy*dy)

        length = length * (n_pillars-1) # total lenght of the liftline considering similar conditions and spacing between pillars

        return n_pillars,F_tension,anchor_angles,length



if __name__ == "__main__" : 
    import matlab.engine 
    g = 9.81 
    u_cart = 2.2 # in [m/s]
    pph = 750 # people per hour 
    ppc = 2 # people per cart 
    ver_diff = 1999.8 - 1818.0 # vertical distance between the 2 lift stations [m]
    hor_diff = 858.7 # horizontal distance between the 2 lift stations [m]
    wpm_wr = 0.25 * g # wire rope weight per meter [N/m]
    wpp = 170 * g # person weight with equipment [N]

    wpc_dry = 300 * g # "dry" cart weight (only the cart) [N]
    wpc_wet  = wpp*ppc + wpc_dry # fully loaded "wet" cart weight [N]

    problem_data = [u_cart,pph,ppc,ver_diff,hor_diff]
    user_data = [wpm_wr,wpp,wpc_dry,wpc_wet]


    # Using identical pillars, spaced at identical hor and vert distances 
    pillar_height = 6 
    height_margin = 3 
    my_pillars = pillar_group(problem_data,user_data)
    eng = matlab.engine.start_matlab()
    n_pillars,wr_disp,anchor_angles,line_length = my_pillars.number(48,0.05,eng,9)
    anchor_angles = [float(num) for num in anchor_angles[0]]
    print(np.rad2deg(anchor_angles))
