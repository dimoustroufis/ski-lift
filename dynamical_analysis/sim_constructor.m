%% Load friction Parameters

% Define the time range and ramp parameters
t_a = 10;  % Time duration of accelaation
final_vel_value = 2.2;  % Velocity value

% Generate the time vector
t = 0:0.1:20;  % Adjust the time step and range as needed

% Generate the ramp signal
ramp = final_vel_value/t_a * t;  % The ramp increases linearly with time

% Set the values above Q to a constant value of Q
ramp(ramp > final_vel_value) = final_vel_value;

% Plot the ramp signal
% plot(t, ramp);
% xlabel('Time');
% ylabel('Amplitude');
% title('Ramp Signal');
% grid on;


vel_prof =  timeseries(ramp,t);
save("vel_prof.mat","vel_prof","-v7.3");
%
damping = 0.1 ; % [N*s/m]