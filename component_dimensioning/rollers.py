from scipy.optimize import fsolve
import numpy as np 
import matplotlib.pyplot as plt 



class roller(): 

    def __init__(self, r_g ,D_i ,D_wr, count, miu, SP_max=0): 
        self.D_wr = D_wr # diameter of the wire rope [mm]
        self.D_i = D_i # inner diameter of the roller [mm]
        self.r_g = r_g # groove radius
        # self.h1 = h1 # h1 factor according to DIN15020
        # self.h2 = h2 # h2 factor according to DIN15020
        self.miu = miu # bearing coeffficient of friction  
        self.count = count # number of rollers

        
    def fr_coeff(self,F_vert_total,D_o):
        # TODO : Enhance the function to work with non-vertical loads
        F_vert_single = F_vert_total / self.count # force per roller - assumed to be vertical [N]
        phi = np.arctan(self.miu)
        F_fr_r = F_vert_single # resultant friction force
        F_friction = F_fr_r * np.sin(phi) * self.D_i / D_o 

        print(F_friction - F_vert_single*self.miu)

        # calculate enhanced coefficient of friction
        coeff = F_friction/F_vert_single
        if(coeff < self.miu):
            print("ERROR : Friction coefficient is less than bearing friction coefficient \n")
        return coeff
        

    
    
    def groove_pressure(self,F_tension,D_o,D_wr_s,n_strands,theta_wrap): 
        # Calculates the surface pressure on the groove of the sheave
        # the formulas are provided in a manufacturer's whitepaper. https://www.wshampshire.com/wp-content/uploads/timco-sheave-design-1.pdf
        #  Since there is a formula for circumferential load and another formula for point load, we choose the one for the circumferential load because it involves the wrap angle
        # Also unit conversions to the imperial system are needed.
        # D_o refers to the rood diameter of the sheave
        # theta_wrap is expected to be in radians

        D_o = D_o * 0.0393701 # in [in]
        D_wr_s = D_wr_s * 0.0393701 # in [in]
        r_g = self.r_g * 0.0393701 # in [in]

        # determine tension force based on wrap angle 
        F_res = 2 * F_tension * np.sin(theta_wrap/2) * 0.2248 # in [lbs]

        # determine specific pressure
        p_specific = 650 * np.sqrt(((2*r_g - D_wr_s)*F_res)/(2*r_g*D_wr_s*D_o)) # in [psi]

        # determine correction coefficient X
        X = 0 
        if(p_specific <= 7250): 
            X = n_strands 
        elif (p_specific > 7250 and p_specific <= 22000): 
            X = 6 
        elif (p_specific > 22000 and p_specific<=43500): 
            X = 4 
        else :
            X = 2.5 

        # print("The value of specific pressure is %lf [psi]"%p_specific)
        # X = float(input("Insert X based on the value of the specific pressure :  "))

        p_groove = p_specific * np.sqrt(X/n_strands) 
        # print("The value of the surface pressure is %lf [psi]"%p_groove)

        # check for failure 
        # max_groove_press = 32925 # hard-coded value [psi] for 2.2 m/s and 70F temperature
        P_groove_max = 35000 # hard-coded value [psi] for 2.2 m/s and 70F temperature
        if (P_groove_max < p_groove): 
            print(p_groove)
            raise Exception("Sheave groove pressure is %lf psi bigger than the limit"%(p_groove-P_groove_max))

        return p_groove
    
    def bore_pressure(self,F_tension,theta_wrap,n_bearings,w_bearing,Do_bearing): 
        # Calculates the surface pressure on the bore of the sheave based on formulas provided in a manufacturer's whitepaper. https://www.wshampshire.com/wp-content/uploads/timco-sheave-design-1.pdf 
        # no unit conversions to imperial are needed 
        F_res = 2 * F_tension * np.sin(theta_wrap/2) 
        p_bore = F_res / Do_bearing / w_bearing / n_bearings 
        # print("The value of the bore pressure is %lf [N/mm^2]"%p_bore)

        P_bore_max = 35 # hard-coded value [N/mm^2] for less than 70F temperature
        if (P_bore_max < p_bore): 
            print(p_bore)
            raise Exception("Sheave bore pressure is %lf MPa bigger than the limit"%(p_bore-P_bore_max))

        return p_bore



    def wrap_angles(self,pivot_angle,angle_in,angle_out=np.pi/2):
        # Calculates the resulting wrap angle after allowing a pre-determined pivot angle. wrap_in and _out refer to the wrap angle on the first and second sheave of the stage
        # angle argumetns are considered to be given in rad 
        # this function considers the first sheave pair that the rope encounters. For the seond pair the output arguments must simply be flipped
        # Only tested for an output angle of 90deg (i.e a single stage pivot)
        wrap_in = abs(angle_in + pivot_angle - np.deg2rad(90))
        wrap_out = abs(angle_out - pivot_angle)

        return [wrap_in,wrap_out]


if __name__ == "__main__": 
    D_o = 300
    D_i = 72 
    miu = 0.05
    count = 2 
    SP_max = 35/15 # in [MPa], based on ISO 4309:2017 with a 15 sf
    D_wr = 26 # wire rope diameter in [mm]
    D_wr_s = 6 # wire rope strand diameter 
    n_strands = 6 
    theta_wrap = np.deg2rad(10)
    bearing_width = 19 # bearing width  in [mm]
    r_g = 1.05 * D_wr/2 # sheave groove radius from the manufacturer's whitepaper [mm]
    sheave_group = roller(r_g,D_i,D_wr,2,miu,SP_max) 

    F_tension = 3e5  
    groove_press = sheave_group.groove_pressure(F_tension,D_o,D_wr_s,n_strands,theta_wrap)
    bore_press = sheave_group.bore_pressure(F_tension,theta_wrap,1,bearing_width,D_i) 
