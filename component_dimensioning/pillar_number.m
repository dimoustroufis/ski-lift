function n_pillars = pillar_number(endforce,ver_dist,hor_dist,D_wr,wpm_wr,ch_weight, ch2ch_dist ,n_nodes, n_init, sag_percent,pillar_height,height_margin)
%PILLAR_NUMBER Calls the FEM code to calculate sag and increases the pillar number until an acceptable value is reached. 
    n_pillars = n_init ; 
    hor_dist = hor_dist/(n_pillars-1);
    ver_dist = ver_dist/(n_pillars-1);
    [~,sag,~,~,~] = wire_sim(endforce,ver_dist,hor_dist,D_wr,wpm_wr,ch_weight,ch2ch_dist,n_nodes);
    
    while(sag>hor_dist*sag_percent || sag>(pillar_height - height_margin))
        n_pillars = n_pillars + 1 ; 
        hor_dist = hor_dist/(n_pillars-1);
        ver_dist = ver_dist/(n_pillars-1);

        [~,sag,~,~,~] = wire_sim(endforce,ver_dist,hor_dist,D_wr,wpm_wr,ch_weight,ch2ch_dist,n_nodes);
        disp(sag)
        disp(n_pillars)
    end 

end

